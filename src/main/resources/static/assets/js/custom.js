


function showname() {
    var fullPath = $("#input_file").val();
    if (fullPath === "") {
        document.getElementById('button_file').innerText = "Nahrát soubor s trasou tripu";
    } else {
        var filename = fullPath.replace(/^.*[\\\/]/, '');
        document.getElementById('button_file').innerText = filename;
    }


}

function showGps(data) {
    var selectCountryName = data.options[data.selectedIndex].innerText;
    document.getElementById("nameOfCountry").textContent=data.options[data.selectedIndex].innerText;
    if (selectCountryName === 'Czech Republic' ){
        $("#gpsToShowWhenCz").addClass('showGps');
    }else{
        $("#gpsToShowWhenCz").removeClass('showGps');
        $("#gpsToShowWhenCz").addClass('gps');
    }


}

function getIndexPageFunctions() {
    getNotifications();
    getResolution();
    calculateSideBarWidth();
}

function calculateSideBarWidth() {
    let width = window.innerWidth;
    var salam = document.getElementsByTagName('svg')[0].height;
    let widthOfGTag = salam.animVal.value*1.1385642737;
    let widthOfSideBar = (width-widthOfGTag)/2;
    widthOfSideBar = Math.round(widthOfSideBar);
    document.getElementsByClassName('leftSideBar')[0].setAttribute('style','width:'+widthOfSideBar.toString()+'px')

}



function getResolution() {


    let width = window.innerWidth;
    let height = window.innerHeight;
    if (document.getElementById('world-map')) {
        document.getElementById('world-map').setAttribute("style", "width:" + width + "px; height:" + height + "px;");
    } else {
        document.getElementById('mapaCeska').setAttribute("style", "width:" + width + "px; height:" + height + "px;");
    }
    document.getElementById('content').setAttribute("style", "width:" + width + "px; height:" + height + "px;");
}

function getNotifications() {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/getNotifications",
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#notificationBot').append(data.toString());
            $('#notificationTop').append(data.toString());
        },
        error: function (e) {
            alert(e);
        }
    });
}

function getModalWithData() {
    $.ajax({
        type: "POST",
        contentType: "aplication/html",
        url: "/indexCesko",
        dataType: 'html',
        success: function (data) {
            $('#myModal').modal('show');
            $('#modalForMyOwnContext').html(data);


        }, error: function (e) {
            alert(e);
        }
    })

}

function closeModal() {
    $('#myModal').modal('hide');

}

function fileSelect(evt) {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        var files = evt.files;
        var result = '';
        var file;
        for (var i = 0; file = files[i]; i++) {
            if (file.size > 8388608) {
                //show an alert to the user
                alert("Povolená velikost souboru je Max. 8 MB");

                //reset file upload control
                this.value = null;
            } else {
                result += '<li>' + file.name + ' ' + (Math.round(file.size / 1048576 * 100) / 100).toFixed(2) + ' MB</li>';
            }
        }

        $('#filesInfo').innerHTML = '<ul class="text-decoration-none">' + result + '</ul>';
    } else {
        alert('The File APIs není ve vašem prohlížeči podporováno.');
    }
}


$("select").on("click", function () {

    $(this).parent(".select-box").toggleClass("open");

});

$(document).mouseup(function (e) {
    var container = $(".select-box");

    if (container.has(e.target).length === 0) {
        container.removeClass("open");
    }
});


$("select").on("change", function () {

    var selection = $(this).find("option:selected").text(),
        labelFor = $(this).attr("id"),
        label = $("[for='" + labelFor + "']");

    label.find(".label-desc").html(selection);

});




