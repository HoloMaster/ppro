package com.example.cestovanimara.controllers.viewRoutes;

import java.util.List;

public class RoutesImpl{
    public static final String VIEWS_CREATE_OR_UPDATE_TRIP = "blog-create";
    public static final String VIEWS_ALL_TRIPS = "blog-list";
    public static final String VIEWS_ALL_TRIPS_ADMIN = "blog-list-admin";
    public static final String REDIRECT_TO_ALL_TRIPS = "redirect:/blog-list";
    public static final String VIEW_BLOG_SINGLE ="blog-single";
    public static final String CREATE_INDEX_PAGE = "createIndexPage";
    public static final String ADMIN__CREATE_INDEX_PAGE = "/admin/createIndexPage";
    public static final String REDIRECT__INDEX = "redirect:/index";
    public static final String REDIRECT__ADMIN = "redirect:/admin";
    public static final String INDEX = "index";
    public static final String ERROR = "error";
    public static final String INDEX_CESKO = "indexCesko";
    public static final String PAGES_SIGN_UP = "pages-sign-up";
    public static final String LOGIN = "login";
    public static final String PNG = "png";
    public static final String JPG = "jpg";
    public static final String JPEG = "jpeg";
    public static final String XML = "xml";
    public static final String EMPTY = "";
    public static final String ADMIN = "admin";
    public static final String USER_UPDATE = "userUpdate";
}
