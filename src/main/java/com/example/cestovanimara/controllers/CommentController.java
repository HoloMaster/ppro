package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.CommentRepository;
import com.example.cestovanimara.repository.TripRepository;
import com.example.cestovanimara.model.Comment;
import com.example.cestovanimara.model.Trip;
import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@Data
public class CommentController {

    private final CommentRepository commentRepository;
    private final TripController tripController;
    private final TripRepository tripRepository;


    @PostMapping("/create-comment/{tripId}")
    public String createCommentForm(@Valid Comment comment, BindingResult result, Model model, @PathVariable(value = "tripId") int tripId){
        Object userPrincipal = SecurityContextHolder.getContext().getAuthentication().getName();
        if (result.hasErrors()){

            return"blog-create";
        }else {
            Trip trip = this.tripRepository.findById(tripId);
            comment.setAuthor(userPrincipal.toString());
            comment.setTrip(trip);
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            comment.setDateOfAddition(formatter.format(date));

            List<Comment> comments = new ArrayList<>();
            comments.add(comment);
            trip.setComments(comments);

            this.commentRepository.save(comment);

            this.tripRepository.save(trip);


            return "redirect:/blog-single/"+tripId;
        }

    }
}
