package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.AdminIndexRepository;
import com.example.cestovanimara.repository.TripRepository;
import com.example.cestovanimara.model.*;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Map;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;

@Controller
@Data
public class AdminController {


    private final AdminIndexRepository adminIndexRepository;
    private final TripRepository trips;

    @GetMapping("/admin/createIndexPage")
    public String initCreationForm(Map<String, Object> model) {
        AdminIndex adminIndex = new AdminIndex();
        model.put("adminIndex", adminIndex);
        return CREATE_INDEX_PAGE;
    }


    @PostMapping("/admin/createIndexPage")
    public String createBlogForm(@Valid AdminIndex adminIndex, BindingResult result,
                                 Model model) throws ParseException {

        if (result.hasErrors()){
            return ADMIN__CREATE_INDEX_PAGE;
        }

            adminIndexRepository.deleteAdminIndex();
            adminIndexRepository.save(adminIndex);

            return REDIRECT__INDEX;
        }

    @PostMapping(value = "/getNotifications", produces =  MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Integer getNotifications(){
        return  this.trips.findAllByApprovedNot();

    }

    }




