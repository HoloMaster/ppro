package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.*;
import com.example.cestovanimara.model.*;
import com.example.cestovanimara.model.modelCollections.Users;
import lombok.Data;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;

@Controller
@Data
public class UserController{

   private final UserRepository userRepository;
   private final TripRepository trips;
   private final PhotoRepository photoRepository;
   private final CommentRepository commentRepository;


    @RequestMapping("/admin")
    public String showAllUsers(Map<String, Object> model){
        Users users = new Users();
        users.getUserList().addAll(userRepository.findAll());

        model.put("users", users);

        return ADMIN;
    }
    @RequestMapping("/admin/userUpdate/{userId}")
    public String initUpdateTripForm(Map<String, Object> model, @PathVariable("userId") Integer userId) {
        User user = userRepository.findById(userId);
        model.put("user",user);
        return USER_UPDATE;
    }

    @PostMapping("/admin/userUpdate/{userId}")
    public String processUpdateTripForm(@Valid User user, BindingResult result,
                                        @PathVariable("userId") Integer userId,
                                        @RequestParam("roleName") String roleName){
        if (result.hasErrors()) {
            return USER_UPDATE;
        } else {

            User userOriginal = userRepository.findById(userId);
            userOriginal.setRoleName(roleName);
            if (!userOriginal.getNickName().equals(user.getNickName()) || !userOriginal.getEmail().equals(user.getEmail())){
                userOriginal.setEmail(user.getEmail());
                userOriginal.setNickName(user.getNickName());
            }

            userRepository.save(userOriginal);


            return REDIRECT__ADMIN;
        }
    }

    @GetMapping("/admin/{userId}/delete")
    public String deleteUser(@PathVariable("userId")Integer userId, Model model) {

        String userPrincipal = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findById(userId);

        User userToBeChanged = userRepository.findByName(userPrincipal);


        if (!user.getTrips().isEmpty()){

            for (Trip trip:
                    user.getTrips()) {
                trip.setUser(userToBeChanged);

                trips.save(trip);
            }
        }


        userRepository.deleteById(userId);

        return REDIRECT__ADMIN;
    }

}
