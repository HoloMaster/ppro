package com.example.cestovanimara.controllers;

    import com.example.cestovanimara.repository.*;
    import com.example.cestovanimara.model.AdminIndex;
    import  com.example.cestovanimara.model.modelCollections.Tags;
    import com.example.cestovanimara.model.modelCollections.Trips;
    import com.example.cestovanimara.model.modelCollections.Users;
    import  lombok.Data;
    import  org.springframework.boot.autoconfigure.security.SecurityProperties;

    import org.springframework.stereotype.Controller;
    import org.springframework.web.bind.annotation.*;
    import org.springframework.security.core.annotation.AuthenticationPrincipal;

    import java.util.*;

    import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;
    import static com.example.cestovanimara.utils.Utils.writeToJSONCzGps;

@Controller
public @Data
class HomeController {

    private final UserRepository users;
    private final TripRepository trips;
    private final TagRepository tags;
    private final AdminIndexRepository adminIndexRepository;
    private final GpsRepository gpsRepository;

    @RequestMapping(value ={"/index","/"})
    public String home(@AuthenticationPrincipal SecurityProperties.User user, Map<String, Object> model) {
        Trips trips = new Trips();
        Users users = new Users();
        Tags tags = new Tags();

        trips.getTripList().addAll(this.trips.findAllDesc());
        Integer countryNumber = this.trips.findCountOfCountries();
        users.getUserList().addAll(this.users.findAll());
        tags.getTagList().addAll(this.tags.findAll());

        List<AdminIndex> index = (List<AdminIndex>) adminIndexRepository.findAll();

        List<String> collectionOfGps = new ArrayList<>();


        model.put("trips", trips);
        model.put("users", users);
        model.put("gps", collectionOfGps);
        model.put("countries", countryNumber);
        if (!index.isEmpty()) {
            model.put("adminIndex", index.get(0));
        }
        return INDEX;
    }

    @RequestMapping(value = "/indexCesko")
    public String  indexCesko(Map<String,Object> model){
        Trips trips = new Trips();
        Users users = new Users();
        Tags tags = new Tags();

        trips.getTripList().addAll(this.trips.findAllWithGps());
        Integer countryNumber = this.trips.findCountOfCountries();
        users.getUserList().addAll(this.users.findAll());
        tags.getTagList().addAll(this.tags.findAll());

        List<AdminIndex> index = (List<AdminIndex>) adminIndexRepository.findAll();

       if (!writeToJSONCzGps(trips.getTripList())){
           return ERROR;
       }

        model.put("trips", trips);
        model.put("users", users);
        model.put("tags", tags);
        //model.put("gps", gpsOfCz);
        model.put("countries", countryNumber);
        if (!index.isEmpty()) {
            model.put("adminIndex", index.get(0));
        }
        return INDEX_CESKO;
    }
}
