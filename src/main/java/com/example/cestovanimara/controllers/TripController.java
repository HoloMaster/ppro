package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.*;
import com.example.cestovanimara.model.*;
import com.example.cestovanimara.model.DTO.TripFilterDTO;
import com.example.cestovanimara.model.modelCollections.Tags;

import com.example.cestovanimara.service.TripService;
import lombok.Data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;


@Controller
public @Data
class TripController {


    private final TripRepository trips;
    private final UserRepository users;
    private final CommentRepository comments;
    private final TagRepository tagRepository;
    private final ServletConfig request;
    private final FileRepository fileRepository;
    private final PhotoRepository photoRepository;
    private final TripService tripService;

    @SuppressWarnings("unchecked")
    @GetMapping("/blog-list")
    public String listOfTrips(Model model, HttpServletRequest request, Pageable pageable,
                              @RequestParam("page") Optional<Integer> page) {

        Collection<String> stringsOfNamesOfCountries = this.trips.findAllTags();

        final int currentPage = page.orElse(0);
        final int size = 10;

        Pageable customPageable = PageRequest.of(currentPage, size);
        Page<Trip> pageOfTrips = this.trips.findAllApprovedOrNot(customPageable, true);


        model.addAttribute("tripsPaged", pageOfTrips);
        model.addAttribute("stringsOfNamesOfCountries", stringsOfNamesOfCountries);
        return VIEWS_ALL_TRIPS;
    }

    @SuppressWarnings("unchecked")
    @GetMapping("/blog-list-admin")
    public String listOfTripsForAdmin(Model model, HttpServletRequest request, Pageable pageable,
                                      @RequestParam("page") Optional<Integer> page) {

        Collection<String> stringsOfNamesOfCountries = this.trips.findAllTags();

        final int currentPage = page.orElse(0);
        final int size = 10;

        Pageable customPageable = PageRequest.of(currentPage, size);
        Page<Trip> pageOfTrips = this.trips.findAllApprovedOrNot(customPageable, false);


        model.addAttribute("tripsPaged", pageOfTrips);
        model.addAttribute("stringsOfNamesOfCountries", stringsOfNamesOfCountries);
        return VIEWS_ALL_TRIPS_ADMIN;
    }

    @PostMapping("/blog-list/filter")
    public String showTrip(@Valid TripFilterDTO tripFilterDTO, BindingResult result, Pageable pageable,
                           @RequestParam("page") Optional<Integer> page, Model model) {

        if (result.hasErrors()){
            return ERROR;
        }
        Collection<String> stringsOfNamesOfCountries = this.trips.findAllTags();

        final int currentPage = page.orElse(0);
        final int size = 10;

        if (tripFilterDTO.getDateFrom().compareTo(tripFilterDTO.getDateTo()) == 1){
               Date flipDate = tripFilterDTO.getDateTo();
               tripFilterDTO.setDateTo(tripFilterDTO.getDateFrom());
               tripFilterDTO.setDateFrom(flipDate);
        }

        Pageable customPageable = PageRequest.of(currentPage, size);
        Page<Trip> tripPage = this.trips.findAllByFilter(customPageable, tripFilterDTO.getCountries(),tripFilterDTO.getDateFrom(),tripFilterDTO.getDateTo());


        model.addAttribute("tripsPaged", tripPage);
        model.addAttribute("stringsOfNamesOfCountries", stringsOfNamesOfCountries);

        return VIEWS_ALL_TRIPS;
    }
    @SuppressWarnings("unchecked")
    @RequestMapping("/blog-list/{country}")
    public String showTrip(@PathVariable("country") String country, Pageable pageable,
                           @RequestParam("page") Optional<Integer> page, Model model) {

        Collection<String> stringsOfNamesOfCountries = this.trips.findAllTags();

        final int currentPage = page.orElse(0);
        final int size = 10;

        Pageable customPageable = PageRequest.of(currentPage, size);

        Page<Trip> tripPage = this.trips.findAllByNameOfCountry(customPageable, country, true);


        model.addAttribute("tripsPaged", tripPage);
        model.addAttribute("stringsOfNamesOfCountries", stringsOfNamesOfCountries);

        return VIEWS_ALL_TRIPS;
    }

    @RequestMapping("/blog-single/{tripId}")
    public ModelAndView showTrip(@PathVariable("tripId") Integer tripId) {
        ModelAndView mav = new ModelAndView(VIEW_BLOG_SINGLE);
        Trip trip = this.trips.findById(tripId);

        mav.addObject(trip);
        return mav;
    }

    @GetMapping("/blog-create")
    public String initCreationForm(Map<String, Object> model) {
        Trip trip = new Trip();
        TagOfTrip tagOfTrip = new TagOfTrip();
        Tags tags = new Tags();
        tags.getTagList().addAll(this.tagRepository.findAll());
        model.put("trip", trip);
        model.put("tag", tagOfTrip);
        model.put("tags", tags);
        return VIEWS_CREATE_OR_UPDATE_TRIP;
    }

    @PostMapping("/blog-create")
    public String createBlogForm(@Valid Trip trip, BindingResult result,
                                 @RequestParam("file_in_web") MultipartFile file_in_web,
                                 @RequestParam("photo_in_web") List<MultipartFile> photo_in_web,
                                 @RequestParam(value = "longLatitude", required = false) Double longLatitude,
                                 @RequestParam(value = "latitude", required = false) Double latitude,
                                 Model model) {

        if (!photo_in_web.isEmpty() && !photo_in_web.get(0).isEmpty() && photo_in_web.get(0) != null) {

            trip.setPhotos(tripService.processPhotos(photo_in_web, trip));
        }

        if (result.hasErrors()) {

            model.addAttribute("trip", trip);

            return VIEWS_CREATE_OR_UPDATE_TRIP;
        } else {
            if (!file_in_web.isEmpty() && file_in_web != null) {

                trip.setFile(tripService.processFile(file_in_web, trip));

            }

            User user = getUsers().findByName(SecurityContextHolder.getContext().getAuthentication().getName());
            if (user.getRoleName().equals("ADMIN")) {
                trip.setApproved(true);
            } else {
                trip.setApproved(false);
            }

            trip.setAuthor(user.getNickName());
            trip.setUser(user);
            trip.setDateOfAddition(new Date());


            if (latitude != null && longLatitude != null) {
                Gps gps = new Gps();
                gps.setLongLatitude(longLatitude);
                gps.setLatitude(latitude);
                gps.setTripWithPointOfInterest(trip);
                trip.setPointOfInterest(gps);
            }


            this.trips.save(trip);

            return REDIRECT_TO_ALL_TRIPS;
        }

    }

    @GetMapping("/blog-list/{tripId}/delete")
    public String deleteTrip(@PathVariable("tripId") Integer tripId, Model model) {
        Trip trip = trips.findById(tripId);
        List<Photo> photos = trips.findById(tripId).getPhotos();
        File file = trips.findById(tripId).getFile();
        if (file != null) {
            java.io.File tripWay = new java.io.File("./uploads/" + file.getNameOfFile());
            tripWay.delete();
        }
        for (Photo photo : photos) {
            java.io.File photoAtWeb = new java.io.File("./uploads/" + photo.getNameOfPhoto());
            photoAtWeb.delete();

            photoRepository.deletePhotoById(photo.getPhoto_id());
        }

        if (!trip.getComments().isEmpty()) {
            List<Integer> idsOfComments = new ArrayList<>();
            for (Comment c : trip.getComments()) {
                idsOfComments.add(c.getComment_id());
            }
            comments.deleteById(idsOfComments);
        }

        trips.deleteTripById(tripId);
        return REDIRECT_TO_ALL_TRIPS;
    }

    @RequestMapping("/blog-list/{tripId}/approve")
    public String approveTripByAdmin(@PathVariable("tripId") Integer tripId) {
        Trip trip = trips.findById(tripId);
        trip.setApproved(true);
        trips.save(trip);

        return REDIRECT_TO_ALL_TRIPS;
    }

    @GetMapping("/blog-list-all/{allIds}/approve")
    public String approveTripsByAdmin(@PathVariable("allIds") String allIds)
    {
        if (allIds.isEmpty())
        {
            return ERROR;
        }

        String[] tripIds = allIds.split("-");

        for (String id : tripIds)
        {
            Integer newId = Integer.parseInt(id);

            Trip trip = trips.findById(newId);
            trip.setApproved(true);
            trips.save(trip);
        }

        return REDIRECT_TO_ALL_TRIPS;
    }


    @RequestMapping("/blog-update/{tripId}")
    public ModelAndView initUpdateTripForm(@PathVariable("tripId") Integer tripId) {
        ModelAndView mav = new ModelAndView("blog-update");
        Trip trip = this.trips.findById(tripId);
        File file = trip.getFile();
        User user = trip.getUser();
        if (file == null){
            file = new File();
        }
        mav.addObject(trip);
        mav.addObject(file);
        mav.addObject(user);
        return mav;
    }

    @PostMapping("/blog-update/{tripId}")
    public String processUpdateTripForm(@Valid Trip trip, BindingResult result, @RequestParam("fileId") Integer fileId,
                                        @RequestParam("userId") Integer userId,
                                        @PathVariable("tripId") Integer tripId) {
        if (result.hasErrors()) {
            return VIEWS_CREATE_OR_UPDATE_TRIP;
        } else {
            Trip trip1 = trips.findById(tripId);
            if (trip.getDateOfAddition() == null || trip.getComments() == null) {
                trip.setDateOfAddition(trip1.getDateOfAddition());
                trip.setComments(trip1.getComments());
            }
            List<Photo> photos = trips.findById(tripId).getPhotos();
            trip.setPhotos(photos);
            trip.setFile(fileRepository.findById(fileId));
            trip.setUser(users.findById(userId));
            this.trips.save(trip);
            return REDIRECT_TO_ALL_TRIPS;
        }
    }

}
