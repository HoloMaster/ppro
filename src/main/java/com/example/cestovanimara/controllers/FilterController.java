package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.*;
import com.example.cestovanimara.model.*;
import com.example.cestovanimara.model.DTO.TripFilterDTO;

import lombok.Data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.ServletConfig;
import javax.validation.Valid;
import java.util.*;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;


@Controller
public @Data
class FilterController {



    private final TripRepository trips;
    private final UserRepository users;
    private final CommentRepository comments;
    private final TagRepository tagRepository;
    private final ServletConfig request;
    private final FileRepository fileRepository;

    private final StorageService storageService;


    @GetMapping("/filter-by-country/{countryName}")
    public String listOfTrips(@Valid TripFilterDTO tripFilterDTO, Pageable pageable,
                              @RequestParam("page") Optional<Integer> page, Model model) {

        Collection<String> stringsOfNamesOfCountries = this.trips.findAllTags();

        final int currentPage = page.orElse(0);
        final int size = 10;

        if (tripFilterDTO.getDateFrom().compareTo(tripFilterDTO.getDateTo()) == 1){
            Date flipDate = tripFilterDTO.getDateTo();
            tripFilterDTO.setDateTo(tripFilterDTO.getDateFrom());
            tripFilterDTO.setDateFrom(flipDate);
        }

        Pageable customPageable = PageRequest.of(currentPage, size);
        Page<Trip> tripPage = this.trips.findAllByFilter(customPageable, tripFilterDTO.getCountries(),tripFilterDTO.getDateFrom(),tripFilterDTO.getDateTo());


        model.addAttribute("tripsPaged", tripPage);
        model.addAttribute("stringsOfNamesOfCountries", stringsOfNamesOfCountries);

        return VIEWS_ALL_TRIPS;
    }



}
