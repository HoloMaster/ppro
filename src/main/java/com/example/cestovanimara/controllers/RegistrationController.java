package com.example.cestovanimara.controllers;

import com.example.cestovanimara.repository.RoleRepository;
import com.example.cestovanimara.repository.UserRepository;
import com.example.cestovanimara.model.User;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;

@Controller
public @Data
class RegistrationController {

    private final UserRepository users;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    @GetMapping("/pages-sign-up")
    public String initCreationForm(Map<String, Object> model) {
        User user = new User();
        model.put("user", user);
        return PAGES_SIGN_UP;
    }

    @PostMapping("/pages-sign-up")
    public String createBlogForm(@Valid User user, BindingResult result){

        if (users.findByName(user.getNickName()) != null){
            result.addError(new FieldError("user","nickName","Toto jméno již existuje"));
        }

        if (result.hasErrors()){


            return PAGES_SIGN_UP;
        }else {
            user.setRoleName(roleRepository.findByName("USER").getRoleName());
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            this.users.save(user);
            return REDIRECT__INDEX;
        }

    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return LOGIN;
    }

}
