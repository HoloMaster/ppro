package com.example.cestovanimara;

import com.example.cestovanimara.utils.StorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EntityScan(basePackages = "/com/example/cestovanimara/model")
@EnableConfigurationProperties(StorageProperties.class)
public class CestovanimaraApplication {

    public static void main(String[] args) {
        SpringApplication.run(CestovanimaraApplication.class, args);
    }

}
