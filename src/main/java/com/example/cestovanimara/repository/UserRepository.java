package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface UserRepository extends Repository<User, Integer> {

    @Transactional(readOnly = true)
    @Cacheable("users")
    Collection<User> findAll() throws DataAccessException;


    @Transactional(readOnly = true)
    User findById(Integer id);


    @Query("SELECT DISTINCT user FROM User user WHERE user.nickName LIKE :nickName%")
    @Transactional(readOnly = true)
    User findByName(@Param("nickName") String nickName);

    @Query("SELECT DISTINCT user FROM User user WHERE user.nickName LIKE :nickName%")
    @Transactional(readOnly = true)
    Collection<User> findAllWithComment(@Param("nickName") String nickName);

    void save(User user);

    @Query("DELETE FROM User user WHERE user.user_id =:user_id")
    @Transactional
    @Modifying
    void deleteById(@Param("user_id") Integer user_id);

//    @Query("select distinct role from User user " +
//            "join Role role on user.role = role " +
//            "where user.user_id like :user_id")
//    @Transactional(readOnly = true)
//    Role findByRole(@Param("user_id") Integer user_id);



}
