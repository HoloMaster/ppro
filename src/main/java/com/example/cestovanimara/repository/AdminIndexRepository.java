package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.AdminIndex;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface AdminIndexRepository extends Repository<AdminIndex, Integer> {



    @Transactional(readOnly = true)
    AdminIndex findById(Integer id);

    @Transactional(readOnly = true)
    @Cacheable("adminIndex")
    Collection<AdminIndex> findAll() throws DataAccessException;

    void save(AdminIndex adminIndex);

    @Modifying
    @Transactional
    @Query("delete from AdminIndex adminIndex")
    void deleteAdminIndex();





}
