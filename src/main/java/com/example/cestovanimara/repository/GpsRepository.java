package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.Gps;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface GpsRepository extends Repository<Gps,Integer> {

    @Transactional(readOnly = true)
    Gps findById(Integer id);

    @Transactional(readOnly = true)
    @Cacheable("adminIndex")
    Collection<Gps> findAll() throws DataAccessException;


    @Transactional(readOnly = true)
    @Query("select count(gps) from Gps gps")
    Integer getCountOfGps();



    void save(Gps gps);

    @Modifying
    @Transactional
    @Query("delete from Gps gps")
    void deleteAdminIndex();





}
