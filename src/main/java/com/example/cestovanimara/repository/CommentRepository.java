package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.Comment;
import com.example.cestovanimara.model.Trip;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;
import java.util.Collection;
import java.util.List;
public interface CommentRepository extends Repository<Comment, Integer> {


    @Transactional(readOnly = true)
    @Cacheable("comments")
    Collection<Trip> findAll() throws DataAccessException;

    @Transactional(readOnly = true)
    Comment findById(Integer id);

    @Query("SELECT DISTINCT comment FROM Comment comment WHERE comment.trip.name LIKE :trip%")
    @Transactional(readOnly = true)
    Collection<Comment> findByTripName(@Param("trip") String trip);

    void save(Comment comment);

    //void edit(Trip trip);

    @Query("DELETE FROM Comment c where c.comment_id = :id")
    @Transactional
    @Modifying
    void deleteById(@Param("id") List<Integer> id);

}
