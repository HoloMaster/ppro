package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.Trip;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.dao.DataAccessException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface TripRepository extends Repository<Trip, Integer> {

    @Transactional(readOnly = true)
    @Cacheable("trips")
    Collection<Trip> findAll() throws DataAccessException;

    @Transactional(readOnly = true)
    Trip findById(Integer id);


    @Transactional(readOnly = true)
    Page findAll(Pageable pageable);

    @Query("SELECT DISTINCT trip FROM Trip trip WHERE trip.name LIKE :name%")
    @Transactional(readOnly = true)
    Collection<Trip> findByName(@Param("name") String name);

    @Query("SELECT DISTINCT trip.comments FROM Trip trip")
    @Transactional(readOnly = true)
    Collection<Trip> findAllWithComment();

    @Query("SELECT count (trip) from Trip trip where trip.approved = false")
    @Transactional(readOnly = true)
    Integer findAllByApprovedNot();

    @Query("select count (trip.author) from Trip trip where trip.author like :author%")
    @Transactional(readOnly = true)
    Integer findAllByAuthor(@Param("author") String author);

    @Query("select distinct count (trip.nameOfCountry) from Trip trip where trip.approved = true")
    @Transactional(readOnly = true)
    Integer findCountOfCountries();


    @Query("select trip from Trip trip order by trip.dateOfAddition desc")
    @Transactional(readOnly = true)
    Collection<Trip> findAllDesc();

    @Query("select trip from Trip trip where trip.pointOfInterest is not null")
    @Transactional(readOnly = true)
    Collection<Trip> findAllWithGps();

    @Query("select trip from Trip trip where trip.approved = :yesOrNot")
    @Transactional(readOnly = true)
    Page findAllApprovedOrNot(Pageable pageable, @Param("yesOrNot") boolean yesOrNot);

    @Query("select trip from Trip trip where trip.nameOfCountry like :countryName AND trip.approved = :yesOrNot")
    @Transactional(readOnly = true)
    Page findAllByNameOfCountry(Pageable pageable, @Param("countryName") String countryName, @Param("yesOrNot") boolean yesOrNot);

    @Query("select distinct trip.nameOfCountry from Trip trip order by trip.nameOfCountry asc")
    @Transactional(readOnly = true)
    Collection<String> findAllTags();


    void save(Trip trip);

    @Query("DELETE FROM Trip trip WHERE trip.trip_id =:id")
    @Modifying
    @Transactional
    void deleteTripById(@Param("id") Integer id);

    @Query("DELETE FROM Trip trip where trip.trip_id =:id")
    @Transactional
    @Modifying
    void deleteByIds(@Param("id") List<Integer> id);

    @Query("select trip from Trip trip where trip.nameOfCountry in :countriesNames " +
            "and trip.dateOfAddition between :dateFrom and :dateTo")
    @Transactional(readOnly = true)
    Page<Trip> findAllByFilter(Pageable customPageable, @Param("countriesNames") List<String> countriesNames,
                               @Param("dateFrom") Date dateFrom,@Param("dateTo") Date dateTo);
}
