package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.File;
import com.example.cestovanimara.model.Photo;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface PhotoRepository extends Repository<Photo, Integer> {


    @Transactional(readOnly = true)
    @Cacheable("photos")
    Collection<Photo> findAll() throws DataAccessException;

    @Transactional(readOnly = true)
    Photo findById(Integer id);

    void save(Photo file);

    List<Photo> saveAll(Iterable<Photo> entities);

    @Query("select photo from Photo photo where photo.trip = :trip_id")
    @Transactional(readOnly = true)
    Collection<Photo> findAllByTrip(@Param("trip_id") Integer trip_id);

    @Query("DELETE FROM Photo photo WHERE photo.trip =:trip_id")
    @Modifying
    @Transactional
    void deletePhotoAssWithTripById(@Param("trip_id") Integer trip_id);

    @Query("DELETE FROM Photo photo WHERE photo.photo_id =:photo_id")
    @Modifying
    @Transactional
    void deletePhotoById(@Param("photo_id") Integer photo_id);

    @Query("SELECT DISTINCT photo FROM Photo photo WHERE photo.nameOfPhoto LIKE :nameOfPhoto%")
    @Transactional(readOnly = true)
    File findByName(@Param("nameOfPhoto") String nameOfPhoto);


}
