package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.TagOfTrip;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;




public interface TagRepository extends Repository<TagOfTrip, Integer> {

    @Transactional(readOnly = true)
    @Cacheable("tags")
    Collection<TagOfTrip> findAll() throws DataAccessException;


    @Transactional(readOnly = true)
    TagOfTrip findById(Integer id);

    void save(TagOfTrip tag);

    List<TagOfTrip> saveAll(Iterable<TagOfTrip> entities);

    @Query("DELETE FROM TagOfTrip tag WHERE tag.tag_id =:tag_id")
    @Transactional(readOnly = true)
    void deleteTripById(@Param("tag_id") Integer tag_id);

}
