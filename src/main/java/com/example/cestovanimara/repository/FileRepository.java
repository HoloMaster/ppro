package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.File;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface FileRepository extends Repository<File, Integer> {


    @Transactional(readOnly = true)
    @Cacheable("files")
    Collection<File> findAll() throws DataAccessException;

    @Transactional(readOnly = true)
    File findById(Integer id);

    void save(File file);

    List<File> saveAll(Iterable<File> entities);

    @Query("DELETE FROM File file WHERE file.file_id =:file_id")
    @Transactional(readOnly = true)
    void deleteFileById(@Param("file_id") Integer file_id);

    @Query("SELECT DISTINCT file FROM File file WHERE file.nameOfFile LIKE :nameOfFile%")
    @Transactional(readOnly = true)
    File findByName(@Param("nameOfFile") String nameOfFile);


}
