package com.example.cestovanimara.repository;

import com.example.cestovanimara.model.Role;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public interface RoleRepository  extends Repository<Role, Integer> {

    @Transactional(readOnly = true)
    @Cacheable("users")
    Collection<Role> findAll() throws DataAccessException;

    @Transactional(readOnly = true)
    Role findById(Integer id);

    @Query("SELECT DISTINCT role FROM Role role WHERE role.roleName LIKE :roleName%")
    @Transactional(readOnly = true)
    Role findByName(@Param("roleName") String roleName);



}
