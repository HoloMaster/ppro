package com.example.cestovanimara.model.DTO;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public @RequiredArgsConstructor
class TripFilterDTO {

    private List<String> countries;
    @DateTimeFormat(pattern = "dd-MM-YYYY")
    private Date dateFrom;
    @DateTimeFormat(pattern = "dd-MM-YYYY")
    private Date dateTo;

}
