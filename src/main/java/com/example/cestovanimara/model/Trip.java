package com.example.cestovanimara.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tblTrip")
@Getter
@Setter
@RequiredArgsConstructor
public class Trip implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer trip_id;


	@NotBlank(message = "Hodnota musí být vyplněna")
	@Size(max = 250)
	String name;

	@NotBlank
	String description;

	String author;

	@NotBlank
	String nameOfCountry;

	boolean approved;

	@NotBlank
	@Lob
	String post;

	@DateTimeFormat(pattern = "dd-MM-YYYY")
	Date dateOfAddition;

	@OneToMany(mappedBy = "trip",cascade = CascadeType.PERSIST)
	List<Comment> comments;

	@OneToMany(mappedBy = "trip")
	List<TagOfTrip> tags;

	@ManyToOne(fetch = FetchType.EAGER)
	private User user;

	@OneToOne(cascade = CascadeType.PERSIST)
	File file;

	@OneToMany(mappedBy = "trip",cascade = CascadeType.PERSIST)
	List<Photo> photos;


	@OneToOne(cascade = CascadeType.PERSIST)
	Gps pointOfInterest;


	public boolean isNew() {
		return this.trip_id == null;
	}



}
