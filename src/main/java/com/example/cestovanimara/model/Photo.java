package com.example.cestovanimara.model;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tblPhoto")
@Getter
@Setter
@RequiredArgsConstructor
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int photo_id;

    private String nameOfPhoto;

    private String originalNameOfPhoto;

    private String pathToFile;

    @ManyToOne(cascade = CascadeType.PERSIST)
    Trip trip;
}
