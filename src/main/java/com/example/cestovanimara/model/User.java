package com.example.cestovanimara.model;

import lombok.Data;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tblUser")
@Getter
@Setter
@RequiredArgsConstructor
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int user_id;

	@NotBlank
	@Size(min = 1,max = 20)
	@Pattern(regexp = "[a-zA-z0-9-_.]+[^<>]",message = "Povolené znaky pro pole jsou A-Z/0-9/-/./_")
	private String nickName;

	@NotBlank
	@Email
	private String email;

	private int travelPoints;

	@NotBlank
	private String password;

	private String roleName;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	private List<Trip> trips;


}
