package com.example.cestovanimara.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "tblComment")
public @Data class Comment implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int comment_id;

	@Lob
	private String textOfComment;

	private String dateOfAddition;

	private String author;

	@ManyToOne(cascade = CascadeType.PERSIST)
	private Trip trip;

	@OneToOne(mappedBy = "comment")
	private Notification notification;

}
