package com.example.cestovanimara.model;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tblGps")
public @Data class Gps implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int gps_id;


	@DecimalMax(value = "180.0")
	@Digits(integer = 3, fraction = 7)
	private double longLatitude;



	@DecimalMax(value = "180.0")
	@Digits(integer = 3, fraction = 7)
	private double latitude;

	@OneToOne(mappedBy = "pointOfInterest", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	private Trip tripWithPointOfInterest;

}
