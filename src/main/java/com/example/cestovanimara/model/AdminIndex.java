package com.example.cestovanimara.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "tblAdminIndex")
@Setter
@Getter
@RequiredArgsConstructor
public class AdminIndex {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int adminIndex_id;

    @Lob
    private String textOfIndex;


}
