package com.example.cestovanimara.model;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tblFile")
@Getter
@Setter
@RequiredArgsConstructor
public class File implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int file_id;

    private String nameOfFile;

    private String originalNameOfFile;

    private String pathToFile;

    @OneToOne(mappedBy = "file", cascade = CascadeType.PERSIST)
    Trip trip;

}
