package com.example.cestovanimara.model;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "tblNotification")
public @Data class Notification implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int notification_id;

	@NotNull
	private String description;

	@NotNull
	private String subject;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dateOfCreation;

	@OneToOne
	private Comment comment;

}
