package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.Gps;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Gpss {
    private List<Gps> gpsList;

    @XmlElement
    public List<Gps> getGpsList(){
        if (gpsList == null){
            gpsList = new ArrayList<>();
        }
        return gpsList;
    }
}
