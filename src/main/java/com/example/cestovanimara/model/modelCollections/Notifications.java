package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.Notification;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Notifications {
    private List<Notification> notifications;

    @XmlElement
    public List<Notification> getNotificationList(){
        if (notifications == null){
            notifications = new ArrayList<>();
        }
        return notifications;
    }
}