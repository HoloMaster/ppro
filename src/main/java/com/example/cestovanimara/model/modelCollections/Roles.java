package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.Role;
import com.example.cestovanimara.model.TagOfTrip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Roles {
    private List<Role> roles;

    @XmlElement
    public List<Role> getTagList(){
        if (roles == null){
            roles = new ArrayList<>();
        }
        return roles;
    }
}
