package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.Comment;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Comments {
    private List<Comment> comments;

    @XmlElement
    public List<Comment> getCommentList(){
        if (comments == null){
            comments = new ArrayList<>();
        }
        return comments;
    }
}
