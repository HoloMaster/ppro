package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.TagOfTrip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Tags {
    private List<TagOfTrip> tags;

    @XmlElement
    public List<TagOfTrip> getTagList(){
        if (tags == null){
            tags = new ArrayList<>();
        }
        return tags;
    }
}
