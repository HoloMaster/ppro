package com.example.cestovanimara.model.modelCollections;

import com.example.cestovanimara.model.Trip;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Trips {
    private List<Trip> trips;

    @XmlElement
    public List<Trip> getTripList(){
        if (trips == null){
            trips = new ArrayList<>();
        }
        return trips;

    }
}
