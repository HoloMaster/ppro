package com.example.cestovanimara.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Table(name = "tblTag")
public @Data class TagOfTrip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tag_id;

    @NotNull(message = "Hodnota musí být vyplněna")
    private String name;

   @ManyToOne
   private Trip trip;



   public String [] getTagsFromInput(String in){
    String delimiter = "[,]";
       return in.split(delimiter);
   }
}
