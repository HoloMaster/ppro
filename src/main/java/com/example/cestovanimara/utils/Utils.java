package com.example.cestovanimara.utils;

import com.example.cestovanimara.model.Trip;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import static com.example.cestovanimara.controllers.viewRoutes.RoutesImpl.*;

public class Utils {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String getTypeOfFile(String file) {

        String[] tokens = file.split("[.]");

        for (int i = 0; i < tokens.length; i++) {
            if (tokens[i].equals("png")) return PNG;
            if (tokens[i].equals("jpg")) return JPG;
            if (tokens[i].equals("jpeg")) return JPEG;
            if (tokens[i].equals("gpx")) return XML;

        }
        return EMPTY;
    }

    public static String randomAlphaNumeric(int fotoNumber)
    {
        StringBuilder builder = new StringBuilder();

        Timestamp ts = new Timestamp(System.currentTimeMillis());
        builder.append(fotoNumber);
        builder.append('-');
        builder.append(ts.getTime());

        return builder.toString();
    }


    public static void resize(String fileName, String typeOfFile) throws IOException {
        BufferedImage temp = null;
        int widthOrHeight = 1920;

        //System.out.println("File " + fileName);
        Image img = ImageIO.read(new File("./uploads/" + fileName));
        double aspectRatio = (double) img.getWidth(null) / (double) img.getHeight(null);

        if (aspectRatio > 1) {
            temp = resizeImage(img, widthOrHeight, (int) (widthOrHeight / aspectRatio));
        } else {
            temp = resizeImage(img, (int) (widthOrHeight / aspectRatio), widthOrHeight);
        }
        File newFile = new File("./uploads/" + fileName);
        ImageIO.write(temp, typeOfFile, newFile);


    }

    static BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }


    @SuppressWarnings("unchecked")
    public static boolean writeToJSONCzGps(List<Trip> trips) {


        JSONArray employeeList = new JSONArray();
        for (Trip trip : trips) {
            StringBuilder gpsOfCz = new StringBuilder("");
            JSONObject employeeDetails = new JSONObject();
            employeeDetails.put("id", trip.getTrip_id());
            employeeDetails.put("name", trip.getName());
            gpsOfCz.append(trip.getPointOfInterest().getLongLatitude()).
                    append("N").append(",").append(trip.getPointOfInterest().getLatitude()).append("E");
            employeeDetails.put("coords", gpsOfCz.toString());

            employeeList.add(employeeDetails);

        }

        //Write JSON file
        try (FileWriter file = new FileWriter("./uploads/gpsOfCz.json")) {

            file.write(employeeList.toJSONString());
            file.flush();
            return true;

        } catch (IOException e) {
            return false;
        }
    }

}
