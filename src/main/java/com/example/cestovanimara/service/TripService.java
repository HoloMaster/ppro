package com.example.cestovanimara.service;

import com.example.cestovanimara.repository.StorageService;
import com.example.cestovanimara.model.Photo;
import com.example.cestovanimara.model.Trip;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.cestovanimara.utils.Utils.*;

@Service
public @Data
class TripService {

    private final StorageService storageService;
    private String nameOfPhotos;
    private String originalName;

    public List<Photo> processPhotos(List<MultipartFile> photo_in_web, Trip trip) {
        List<Photo> photoToBeSaved = new ArrayList<>();
        int i = 0;
        for (MultipartFile file : photo_in_web) {

            originalName = file.getOriginalFilename();
            nameOfPhotos = storageService.store(file, randomAlphaNumeric(i) + "." + getTypeOfFile(file.getOriginalFilename()));
            try {
                resize(nameOfPhotos, getTypeOfFile(file.getOriginalFilename()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Photo photo = new Photo();
            String uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/uploads/" + nameOfPhotos).toUriString();

            photo.setTrip(trip);
            photo.setNameOfPhoto(nameOfPhotos);
            photo.setOriginalNameOfPhoto(originalName);
            photo.setPathToFile(uri);
            photoToBeSaved.add(photo);

            i++;
        }

        return photoToBeSaved;
    }

    public com.example.cestovanimara.model.File processFile(MultipartFile file_in_web, Trip trip) {
        com.example.cestovanimara.model.File fileToBeSaved = new com.example.cestovanimara.model.File();
        originalName = file_in_web.getOriginalFilename();
        String name = storageService.store(file_in_web, randomAlphaNumeric(11) + getTypeOfFile(file_in_web.getOriginalFilename()));
        String uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/uploads/" + name).toUriString();

        fileToBeSaved.setTrip(trip);
        fileToBeSaved.setNameOfFile(name);
        fileToBeSaved.setOriginalNameOfFile(originalName);
        fileToBeSaved.setPathToFile(uri);

        return fileToBeSaved;

    }
}
