package com.example.cestovanimara;


import com.example.cestovanimara.controllers.RegistrationController;
import com.example.cestovanimara.repository.RoleRepository;
import com.example.cestovanimara.repository.UserRepository;
import com.example.cestovanimara.model.User;
import com.example.cestovanimara.securingweb.UserPrincipalDetailImp;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
public class TripRepositoryIntegrationTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserPrincipalDetailImp userPrincipalDetailImp;


    /**
     * return 200 because user data are correctly send to correct form and correctly saved
     *
     * @throws Exception
     */
    @Test
    public void  whenValidUrlAndMethodAndContentType_thenReturns200() throws Exception {
        User user = new User();
        user.setNickName("jan");
        user.setEmail("jan@uhk.cz");
        user.setRoleName("USER");

        mockMvc.perform(post("/pages-sign-up")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    /**
     * return 500 because user data are correctly send but form does not exist on that post template
     *
     * @throws Exception
     */
    @Test
    public void  whenValidUrlAndMethodAndContentType_thenReturns500() throws Exception {
        User user = new User();
        user.setNickName("jan");
        user.setEmail("jan@uhk.cz");
        user.setRoleName("USER");

        mockMvc.perform(post("/pages-signup")
                .content(objectMapper.writeValueAsString(user))
                .contentType("application/json"))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    /**
     * Return 404 because page does not exist (only if in database is not blog with this ID)
     *
     * @throws Exception
     */
    @Test
    public void givenGreetURIWithPathVariable_whenMockMVC_thenResponseOK() throws Exception{
        this.mockMvc
                .perform(get("/blog-single/{tripId}", "1"))
                .andDo(print()).andExpect(status().is(404));
    }


}
