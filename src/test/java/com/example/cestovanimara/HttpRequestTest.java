package com.example.cestovanimara;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    /**
     * Test connection to home page
     *
     * @throws Exception
     */
    @Test
    public void testConnectionHome() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("index");
    }

    /**
     * Test connection to page with blogs
     *
     * @throws Exception
     */
    @Test
    public void testConnectionBlogs() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/blog-list",
                String.class)).contains("Filtr");
    }

    /**
     * Test connection to registration page
     *
     * @throws Exception
     */
    @Test
    public void testConnectionRegister() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/pages-sign-up",
                String.class)).contains("email");
    }

    /**
     * Test connection to error page
     *
     * @throws Exception
     */
    @Test
    public void testConnectionError() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/error",
                String.class)).contains("No message");
    }

    /**
     * Test connection to login page
     *
     * @throws Exception
     */
    @Test
    public void testConnectionLogin() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/login",
                String.class)).contains("Login");
    }
}
