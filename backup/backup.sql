-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: cestovani
-- ------------------------------------------------------
-- Server version	5.7.31
create database cestovani;
use cestovani;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_admin_index`
--

DROP TABLE IF EXISTS `tbl_admin_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_admin_index` (
  `admin_index_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_of_index` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`admin_index_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_admin_index`
--

LOCK TABLES `tbl_admin_index` WRITE;
/*!40000 ALTER TABLE `tbl_admin_index` DISABLE KEYS */;
INSERT INTO `tbl_admin_index` VALUES (1,'<p>Jsem dana</p>\r\n');
/*!40000 ALTER TABLE `tbl_admin_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_comment`
--

DROP TABLE IF EXISTS `tbl_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_addition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_of_comment` longtext COLLATE utf8_unicode_ci,
  `trip_trip_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `FK4xpy8tqn4si3hb2gw4yhe8ioh` (`trip_trip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_comment`
--

LOCK TABLES `tbl_comment` WRITE;
/*!40000 ALTER TABLE `tbl_comment` DISABLE KEYS */;
INSERT INTO `tbl_comment` VALUES (1,'NuimiKazushito','26-10-2020','ahoj prde',1);
/*!40000 ALTER TABLE `tbl_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_file`
--

DROP TABLE IF EXISTS `tbl_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_name_of_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_to_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_file`
--

LOCK TABLES `tbl_file` WRITE;
/*!40000 ALTER TABLE `tbl_file` DISABLE KEYS */;
INSERT INTO `tbl_file` VALUES (1,'11-1603738531660xml','export.gpx','http://localhost:8080/uploads/11-1603738531660xml');
/*!40000 ALTER TABLE `tbl_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_gps`
--

DROP TABLE IF EXISTS `tbl_gps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_gps` (
  `gps_id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double NOT NULL,
  `long_latitude` double NOT NULL,
  PRIMARY KEY (`gps_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_gps`
--

LOCK TABLES `tbl_gps` WRITE;
/*!40000 ALTER TABLE `tbl_gps` DISABLE KEYS */;
INSERT INTO `tbl_gps` VALUES (1,15.8405667,50.2116211);
/*!40000 ALTER TABLE `tbl_gps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification`
--

DROP TABLE IF EXISTS `tbl_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_creation` datetime DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment_comment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `FKbwp09chadqnuo6bpl5mi2krua` (`comment_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification`
--

LOCK TABLES `tbl_notification` WRITE;
/*!40000 ALTER TABLE `tbl_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_photo`
--

DROP TABLE IF EXISTS `tbl_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_photo` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_name_of_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path_to_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trip_trip_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`photo_id`),
  KEY `FKe9pl52nd6kpq59c9b4bo305cf` (`trip_trip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_photo`
--

LOCK TABLES `tbl_photo` WRITE;
/*!40000 ALTER TABLE `tbl_photo` DISABLE KEYS */;
INSERT INTO `tbl_photo` VALUES (1,'1603738059530.jpg','15456750357_e584a055c6_z.jpg','http://localhost:8080/uploads/1603738059530.jpg',1),(2,'0-1603738369214.jpg','a4486a7d7a78a0620393fee7d544fab7.jpg','http://localhost:8080/uploads/0-1603738369214.jpg',2),(3,'1-1603738369623.png','Bell-Cranel-01.png','http://localhost:8080/uploads/1-1603738369623.png',2),(4,'0-1603738531362.jpg','older_rem_bride.jpg','http://localhost:8080/uploads/0-1603738531362.jpg',3),(5,'0-1604343678622.jpg','myImage.jpg','http://localhost:8080/uploads/0-1604343678622.jpg',4),(6,'0-1604343832811.jpg','older_rem_bride.jpg','http://localhost:8080/uploads/0-1604343832811.jpg',5),(7,'0-1604343849303.jpg','uKUeqWzkGYdr2w5zEQIz_28_dde2e8d9756d889d75903f02651f0617_image.jpg','http://localhost:8080/uploads/0-1604343849303.jpg',6),(8,'0-1604348375749.jpg','older_rem_bride.jpg','http://localhost:8080/uploads/0-1604348375749.jpg',8),(9,'0-1604348393171.jpg','dckt4ib-e425ae51-8ad0-41ee-9bdf-8d9e45dd01ef.jpg','http://localhost:8080/uploads/0-1604348393171.jpg',9);
/*!40000 ALTER TABLE `tbl_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_tag`
--

DROP TABLE IF EXISTS `tbl_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tag` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trip_trip_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  KEY `FKccrhmwn4ropgnx9e5p7qfks3j` (`trip_trip_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tag`
--

LOCK TABLES `tbl_tag` WRITE;
/*!40000 ALTER TABLE `tbl_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_trip`
--

DROP TABLE IF EXISTS `tbl_trip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_trip` (
  `trip_id` int(11) NOT NULL AUTO_INCREMENT,
  `approved` bit(1) NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_addition` datetime DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_of_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post` longtext COLLATE utf8_unicode_ci,
  `file_file_id` int(11) DEFAULT NULL,
  `point_of_interest_gps_id` int(11) DEFAULT NULL,
  `user_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`trip_id`),
  KEY `FKpger1skw4vn8wcwf6n994aj18` (`file_file_id`),
  KEY `FKer2y1pflv5339w68gdfo7wqcb` (`point_of_interest_gps_id`),
  KEY `FK5hh0imbwv5q42xkyktypcry59` (`user_user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_trip`
--

LOCK TABLES `tbl_trip` WRITE;
/*!40000 ALTER TABLE `tbl_trip` DISABLE KEYS */;
INSERT INTO `tbl_trip` VALUES (1,_binary '','NuimiKazushito','2020-10-26 18:47:40','<p>aaa</p>\r\n','2019-11-14-tInvoice.sql','FR','<p>aaa</p>\r\n',NULL,NULL,4),(2,_binary '','NuimiKazushito','2020-10-26 18:52:50','<p>Vsichni</p>\r\n','Jedeme do nemecka','DE','<p>to si teda piste</p>\r\n',NULL,NULL,4),(3,_binary '','NuimiKazushito','2020-10-26 18:55:32','<p>Uz se moc tesime</p>\r\n','Jedeme za Honzikem','CZ','<p>Bude to dlouha jizda</p>\r\n',1,1,4),(4,_binary '','neco','2020-11-02 19:01:19','<p>111</p>\r\n','test','0','<p>111</p>\r\n',NULL,NULL,3),(5,_binary '','neco','2020-11-02 19:03:53','<p>111</p>\r\n','test','CZ','<p>11</p>\r\n',NULL,NULL,3),(6,_binary '','neco','2020-11-02 19:04:10','<p>11</p>\r\n','test 11','CZ','<p>111</p>\r\n',NULL,NULL,3),(7,_binary '','neco','2020-11-02 20:18:31','<p>1111</p>\r\n','test111','CZ','<p>111</p>\r\n',NULL,NULL,3),(8,_binary '','neco','2020-11-02 20:19:36','<p>111</p>\r\n','111','CZ','<p>111</p>\r\n',NULL,NULL,3),(9,_binary '','neco','2020-11-02 20:19:54','<p>111</p>\r\n','rasd','CZ','<p>1111</p>\r\n',NULL,NULL,3);
/*!40000 ALTER TABLE `tbl_trip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nick_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `travel_points` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;



/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-25 19:01:51
