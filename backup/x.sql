use cestovani;

INSERT INTO tbl_role VALUES (1,'USER');
INSERT INTO tbl_role VALUES (2,'ADMIN');

INSERT INTO `tbl_user` VALUES (3,'neco@neco.cz','neco','$2a$10$dLHseO/B9uXx7ufLiiZ8GOz07wcWHiI4zkG0Wsm1tMS.ZXpdPE1he','ADMIN',0),(4,'NuimiKazushito@sek.si','NuimiKazushito','$2a$10$EJL.1.tQa1XzS/M85ztKluLVXe79RThvizGs4N5NUhWlmcVsk5U8W','ADMIN',0);

