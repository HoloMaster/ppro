FROM openjdk:8
ADD target/cestovanimara.jar cestovanimara.jar

ENV WAIT_VERSION 2.7.3
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait
# Environment variables
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "cestovanimara.jar"]