FROM mysql:5.7

# Environment variables
ENV MYSQL_ROOT_PASSWORD root
#ENV MYSQL_DATABASE cestovani

COPY backup/ /docker-entrypoint-initdb.d/

RUN chown -R mysql:mysql /docker-entrypoint-initdb.d/

CMD ["mysqld", "--character-set-server=utf8mb4", "--collation-server=utf8mb4_unicode_ci"]



